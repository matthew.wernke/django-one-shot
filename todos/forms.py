from django import forms
from todos.models import TodoList, TodoItem


class TodoListForm(forms.ModelForm):
    class Meta:
        model = TodoList
        exclude = []


class TodoItemForm(forms.ModelForm):
    class Meta:
        model = TodoItem
        exclude = []
