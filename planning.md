## Setup/Feature 1
* [x] Fork and clone the starter project from django-one-shot
* [x] Create a new virtual environment in the repository directory for the project
* [x] Activate the virtual environment
* [x] Upgrade pip
* [x] Install django
* [x] Install black
* [x] Install flake8
* [x] Install djlint
* [x] Install django-debug-toolbar
* [x] Deactivate your virtual environment
* [x] Activate your virtual environment
* [x] Use pip freeze to generate a requirements.txt file
  * [x] pip freeze > requirements.txt
* [x] run tests successfuly
* [x] push to git

## Feature 2
* [x] Create a Django project named brain_two so that the manage.py file is in the top directory
* [x] Create a Django app named todos and install it in the brain_two Django project in the INSTALLED_APPS list
* [x] Run the migrations
* [x] Create a super user
* [x] run tests successfuly
* [x] push to git

## Feature 3
* [x] Create TodoList model in todos Django app
  * [x] name: name; type: string; constraints: maximum length of 100 characters;
  * [x] name: created_on; type: datetime; constraints: should be automatically set to the time the todolist was created
  * [x] Moreover, the TodoList model should implicitly convert to a string with the __str__ method that is the value of the name property.
* [x] run migrations
* [x] run tests successfuly
* [x] push to git

## Feature 4
* [x] Register the TodoList model with the admin so that you can see it in the Django admin site.
* [x] run tests successfuly
  * [x] test on admin site
* [x] push to git

## Feature 5
* [x] Create a TodoItem model in the todos Django app.
  * [x] NAME, TYPE, CONSTRAINTS
    * [x] task,	string,	maximum length of 100 characters
    * [x] due_date,	date time,	should be optional
    * [x] is_completed,	boolean,	should default to False
    * [x] list,	foreign key,	should be related to the TodoList model and have a related name of "items". It should automatically delete if the to-do list is deleted. (This is a cascade.)
  * [x] the TodoItem model should implicitly convert to a string with the __str__ method that is the value of the task property.
* [x] run migrations
* [x] shell testing
* [x] script testing
* [x] push to git

## Feature 6
* [x] Register the TodoItem model with the admin so that you can see it in the Django admin site.
* [x] script testing
* [x] push to git

## Feature 7
* [x] Create a view that will get all of the instances of the TodoList model and put them in the context for the template.
  * [x] Register that view in the todos app for the path "" and the name "todo_list_list" in a new file named todos/urls.py.
  * [x] Include the URL patterns from the todos app in the brain_two project with the prefix "todos/".
  * [x] Create a template for the list view that complies with the following specifications.
* [x] Template specifications
  * [x] the fundamental five in it
  * [x] a main tag that contains:
  * [x] div tag that contains:
  * [x] an h1 tag with the content "My Lists"
  * [x] a table that has two columns:
  * [x] the first with the header "Name" and the rows with the names of the Todo lists
  * [x] the second with the header "Number of items" and nothing in those rows because we don't yet have tasks
* [x] test
* [x] push to git

## Feature 8
* [x] Create a view that shows the details of a particular to-do list, including its tasks
* [x] In the todos urls.py file, register the view with the path "<int:pk>/" and the name "todo_list_detail"
* [ ] Create a template to show the details of the todolist and a table of its to-do items
  * [x] the fundamental five in it
  * [x] a main tag that contains:
    * [x] div tag that contains:
      * [x] an h1 tag with the to-do list's name as its content
      * [x] an h2 tag that has the content "Tasks"
      * [x] a table that contains two columns with the headers "Task" and "Due date" with rows for each task in the to-do list
* [x] Update the list template to show the number of to-do items for a to-do list
* [x] Update the list template to have a link from the to-do list name to the detail view for that to-do list
* [x] run tests
* [x] push to git

## Feature 9
* [x] Create a create view for the TodoList model that will show the name field in the form and handle the form submission to create a new TodoList
* [x] If the to-do list is successfully created, it should redirect to the detail page for that to-do list
* [x] Register that view for the path "create/" in the todos urls.py and the name "todo_list_create"
* [x] Create an HTML template that shows the form to create a new TodoList (see the template specifications below)
* [x] Add a link to the list view for the TodoList that navigates to the new create view
* [x] run tests
* [x] push to git

## Feature 10
* [x] run tests
* [x] push to git

## Feature 11
* [x] run tests
* [x] push to git

## Feature 12
* [x] run tests
* [x] push to git

## Feature 13
* [ ] run tests
* [ ] push to git
