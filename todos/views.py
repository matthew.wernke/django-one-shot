from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from todos.models import TodoList, TodoItem
from django.urls import reverse, reverse_lazy
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.


class TodoListView(ListView):
    model = TodoList
    template_name = "todos/list.html"


class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"


class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todos/new.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])


class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/edit.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])


class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    success_url = reverse_lazy("todo_list_list")


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "todos/new_item.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse("todo_list_detail", kwargs={"pk": self.object.list.pk})


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "todos/edit_item.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse("todo_list_detail", kwargs={"pk": self.object.list.pk})
